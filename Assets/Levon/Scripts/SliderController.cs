﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SliderController : MonoBehaviour
{


	public GameObject SliderPanel;
	public int _currentScense = 0;
	void Start()
	{
		PlayerPrefs.SetInt ("level", _currentScense);

	}
	void Update ()
	{
//		if (Input.GetMouseButtonDown (0)) {
//			NextLevel (9);
//		}
//		if (Input.GetMouseButtonDown (1)) {
//			NextLevel (-9);
//		}

	}




	public  void NextLevel (int value)
	{
		if (_currentScense == 0 && value < 0) {
			return;
		}
		if (_currentScense == 2 && value > 0) {
			return;
		}
		if (value > 0) {
			++_currentScense;
		}
		if (value < 0) {
			--_currentScense;
		}
		StopAllCoroutines ();
		StartCoroutine (Go (SliderPanel.transform.position + Vector3.left * value));
		PlayerPrefs.SetInt ("level", _currentScense);
		//print (_currentScense);
	}

	IEnumerator Go (Vector3 _target)
	{
		while (SliderPanel.transform.position != _target) {
			SliderPanel.transform.position = Vector3.MoveTowards (SliderPanel.transform.position, _target, Time.deltaTime * 100);
			yield return null;
		}
	}
}
