﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Leap;
using System;

namespace  TwoPaler
{
	public class GameControllerTwoPlayer : MonoBehaviour
	{

		Controller controller;
		Frame frame;

		public int speed = 5;
		public List<GameObject> ZoneList;
		public GameObject SpawnZone;
		public Text Timer;
		public GameObject Zone;
		public int _currentLevel = 0;

		public enum HandsLoc
		{
			Left,
			Right
		};
		public HandsLoc HLocation = HandsLoc.Left;

		// Use this for initialization
		void Start ()
		{
			controller = new Controller ();
			CreateZone (ZoneList [_currentLevel]);
		}

		/// <summary>
		/// Метод таймера
		/// </summary>
		void TimerMethod ()
		{
			Timer.text = ((int)Time.time).ToString ();
		}

		void _LeapMotion()
		{
			foreach (var item in frame.Hands) {
				if (item.IsRight && HLocation == HandsLoc.Right) {
					if (item.PalmVelocity.z < -50) {
						axisVertcal (true);
					}
					if (item.PalmVelocity.z > 50) {
						axisVertcal (false);
					}
					if (item.PalmVelocity.y < -50) {
						axisHorizontal (false);
					}
					if (item.PalmVelocity.y > 50) {
						axisHorizontal (true);
					}
				}
				if (item.IsLeft && HLocation == HandsLoc.Left) {
					if (item.PalmVelocity.z < -50) {
						axisVertcal (true);
					}
					if (item.PalmVelocity.z > 50) {
						axisVertcal (false);
					}
					if (item.PalmVelocity.y < -50) {
						axisHorizontal (false);
					}
					if (item.PalmVelocity.y > 50) {
						axisHorizontal (true);
					}
				}

			}
		}


		// Update is called once per frame
		void Update ()
		{

			TimerMethod ();
			frame = controller.Frame();
			_LeapMotion ();
		


			if (Input.GetKey (KeyCode.W)) {
				axisVertcal (true);
			}
			if (Input.GetKey (KeyCode.S)) {
				axisVertcal (false);
			}

			if (Input.GetKey (KeyCode.D)) {
				axisHorizontal (true);
			}
			if (Input.GetKey (KeyCode.A)) {
				axisHorizontal (false);
			}
			if (Input.GetKeyDown (KeyCode.Space)) {
				NextStage ();
			}

		}

		/// <summary>
		/// Переключает на следующий лабиринт
		/// </summary>
		void NextStage ()
		{
			if (_currentLevel == 2) {
				Timer.text = "Вы Победили";
				print ("||||||||||||||||||||"); //// вот тут конец всех уровней.
				SceneManager.LoadScene (0);
//			Debug.Break ();
				return;
			}
			Destroy (Zone);
			CreateZone (ZoneList [++_currentLevel]);
		}

		void CreateZone (GameObject _zone)
		{
			Zone = Instantiate (_zone, SpawnZone.transform.position, Quaternion.identity) as GameObject;
		}

		/// <summary>
		/// Поворачивает по вертикали
		/// </summary>
		/// <param name="Vertical">If set to <c>true</c> vertical.</param>
		void axisVertcal (bool Vertical)
		{
			if (Vertical) {
				Zone.transform.Rotate (Vector3.right * speed, Space.World);
			} else if (!Vertical) {
				Zone.transform.Rotate (Vector3.left * speed, Space.World);
			}
		}

		/// <summary>
		/// Поворачивает по горизонтали
		/// </summary>
		/// <param name="Horizontal">If set to <c>true</c> horizontal.</param>
		void axisHorizontal (bool Horizontal)
		{
			if (Horizontal) {
				Zone.transform.Rotate (Vector3.back * speed, Space.World);
			} else if (!Horizontal) {
				Zone.transform.Rotate (Vector3.forward * speed, Space.World);
			}
		}

		/// <summary>
		/// Поворачивает по окружности
		/// </summary>
		/// <param name="Yow">If set to <c>true</c> yow.</param>
		void axisYow (bool Yow)
		{
			if (Yow) {
				Zone.transform.Rotate (Vector3.up * speed, Space.World);
			} else if (!Yow) {
				Zone.transform.Rotate (Vector3.down * speed, Space.World);
			}
		}




	}
}
