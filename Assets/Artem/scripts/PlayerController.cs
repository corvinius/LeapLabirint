﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	[SerializeField] AudioSource katetsay;
	[SerializeField] AudioSource panch;

	// Use this for initialization
	void Start () {
		katetsay = GameObject.Find ("AudioPlayerKat").GetComponent<AudioSource>();
		panch = GameObject.Find ("AudioPlayerUdar").GetComponent<AudioSource> ();
	}
	

	// Update is called once per frame
	void Update () {
		if (this.GetComponent<Rigidbody> ().angularVelocity != Vector3.zero) {
			katetsay.Play();
		} else {
			katetsay.Stop();
		
		}
	}
}
